package ru.brandspot.Mass;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;

import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipViewerActivity extends AppCompatActivity {

    private static final String LOG_APP = "LOG_APP";

    public void overWriteFile(String fileName, String fileJsonString) throws IOException {

        File fileJson = new File(getFilesDir(), fileName);
        if (!fileJson.exists()) {
            Log.d(LOG_APP, fileName + " not exist");

        } else {
            Log.d(LOG_APP, fileName + " exist");

            FileOutputStream fileJsonWrite = openFileOutput(fileName, MODE_PRIVATE);
            fileJsonWrite.write(fileJsonString.getBytes());
            fileJsonWrite.close();
        }

    }


    public String readFile(String fileName) throws IOException {

        File fileJson = new File(getFilesDir(), fileName);
        String fileJsonString = null;
        if (fileJson.exists()) {

            try {
                FileInputStream fileJsonInputStrim = openFileInput(fileName);
                byte[] bytes = new byte[fileJsonInputStrim.available()];
                fileJsonInputStrim.read(bytes);
                fileJsonInputStrim.close();
                fileJsonString = new String (bytes);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fileJsonString;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zipviewer);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        Intent intent = getIntent();

        Toolbar topToolbar = (Toolbar) findViewById(R.id.toolbar);
        topToolbar.setTitle(intent.getStringExtra("fileTitle"));
        setSupportActionBar(topToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        topToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        File fileZip = new File(getFilesDir(), intent.getStringExtra("fileName"));

        String zipDirectory = fileZip.getPath();

        this.unZip(zipDirectory);

        final BridgeWebView webView = (BridgeWebView) findViewById(R.id.webViewZip);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.getAllowContentAccess();
        webSettings.getAllowFileAccessFromFileURLs();
        webSettings.getAllowUniversalAccessFromFileURLs();

        webSettings.getAllowFileAccess();
        webView.setWebChromeClient(new WebChromeClient());

        File file = new File(getFilesDir().getPath() + "/" + destinationDirectory(intent.getStringExtra("fileName")) + "/", "index.html");

        webView.loadUrl("file:///" + file.getPath());



        try {
            JSONObject jsonObj = new JSONObject();
            String fileUserJsonString = this.readFile("user_data.txt");
            jsonObj.put("JSON_STRING_USER", fileUserJsonString);
            webView.send(jsonObj.toString());

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        webView.registerHandler("saveAnswers", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
            Log.d(LOG_APP, "ANSWERS");
            try {
                JSONObject jsonData = new JSONObject(data);
                String jsonNewString = jsonData.getString("DATA_TEST");

                String fileTestsAnswersJsonString = readFile("listTest_file_data.txt");

                overWriteFile(
                        "listTest_file_data.txt",
                        fileTestsAnswersJsonString + jsonNewString + "||\n"
                );

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            }
        });

        webView.registerHandler("closeFromHtml", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {

            Intent mainViewer = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(mainViewer);

            }
        });

    }


    private void unZip(final String zipFileName) {
        byte[] buffer = new byte[1024];

        final String dstDirectory = destinationDirectory(zipFileName);
        final File dstDir = new File(dstDirectory);
        if (!dstDir.exists()) {
            dstDir.mkdir();
        }

        try {
            final ZipInputStream zis = new ZipInputStream(
            new FileInputStream(zipFileName));
            ZipEntry ze = zis.getNextEntry();
            String nextFileName;
            while (ze != null) {
                nextFileName = ze.getName();
                File nextFile = new File(dstDirectory + File.separator
                        + nextFileName);

                if (ze.isDirectory()) {
                    nextFile.mkdir();
                } else {
                    new File(nextFile.getParent()).mkdirs();
                    try (FileOutputStream fos = new FileOutputStream(nextFile)) {
                        int length;
                        while((length = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, length);
                        }
                    }
                }
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ZipViewerActivity.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ZipViewerActivity.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    public String destinationDirectory(final String srcZip) {
        return srcZip.substring(0, srcZip.lastIndexOf("."));
    }

}
