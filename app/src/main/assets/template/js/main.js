;(function () {

    window.App = {};
    App.jsonStr = '';
    App.user = '';
    App.userId = '';
    App.userPass = '';
    App.docPath = '';
    App.dataForNewPoint = '';
    App.updateProcess = false;
    App.dataUpdate = "";
    App.lastUpdate = 0;
    App.langLocal = 'RU';
    App.stringLocal = ['RU', 'EN'];
    App.stringMessageStatus = '';
    App.izbrannoeFiles = "";
    App.lastFiles = "";
    App.listBids = "";
    App.damageCatalog = false;
    App.type_aplication = "Android";
    App.key = "Wert4Df_n1igQ";
    App.usersActions = {
        'OPEN_FILE' : '34',
        'CLOSE' : '35'
    };
    App.typeObj = {
            'APLICATION' : '31',
            'SECTION' : '32',
            'FILE' : '33'
        };

    App.stringLocal['RU'] = {
        'TESTIROVANIE': 'Тестирование',
        'NOVINKI': 'Новинки',
        'LICHNYY-KABINET': 'Личный кабинет',
        'TRENINGI': 'Тренинги',
        'INSTRUMENTY-PRODAZH': 'Инструменты продаж',
        'INFORMATSIYA-PO-BRENDAM': 'Информация по брендам',
        'DETEYLING': 'Детейлинг',
        'SEND': 'Отправить',
        'FILES': 'Файлы',
        'CHAT': 'Чат',
        'FAQ': 'FAQ',
        'FILES_UPLOAD': 'Файлов закачено',
        'FROM': 'из',
        'CHECK_UPLOAD': 'Проверка обновлений',
        'CATALOG_ACTUAL': 'Каталог находится в актуальном состоянии',
        'ERROR_INTERNET': 'Ошибка интернет-соединения.',
        'ERROR_SEND': 'Ошибка отправки сообщения. Проверьте соединение с интернетом и повторите попытку.',
        'ERROR_AUTH': 'Пользователь с такой электронной почтой или паролем не найден!',
        'ERROR': 'Ошибка',
        'ERROR_REG_USER': 'Email уже зарегестрирован.',
        'CHANGE_CATEGORY': 'Выберите категорию',
        'FILL_IN_ALL_FIELDS': 'Заполните все поля',
        'ERROR_EMAIL': 'Неправильно указан email',
        'MES_GOOD_REGISTRATION': 'Спасибо, за регистрацию. В ближайшее время к вам на почту придёт пароль для входа в систему.',
        'REGISTRATION': 'Регистрация',
        'SEND_GOOD_REQUEST': 'Данные были успешно сохранены.',
        'SEND_TITLE_ANKET': 'Отправка анкет на сервер',
        'ERROR_ANKET_REQUEST': 'Ошибка. Возможно отсутствует интерент соединение, данные будут отправленны позже.',
        'FILES': 'Файлы',
        'CHAT': 'Чат',
        'ALL': 'Всего',
        'LOGIN': 'Войти',
        'CHECK_IN': 'Регистрация',
        'SKU': 'SKU',
        'REQUEST_CHANGE_CATALOG': 'Внимание, каталог нуждается в актуализации. Запросить проверку обновлений?',
        'REQUEST_CHANGE_CATALOG_DISABLE_NET': 'Внимание, у вас нет интернет соединения. Запросить проверку обновлений?',
        'PAGE': 'Страница',
        'SEARCH': 'Поиск',
        'LAST_FILES': 'Последние просмотренные файлы',
        'IZBRANNOE': 'Избранное',
        'ERROR_KEY_TITLE': 'Ошибка',
        'ERROR_KEY_DESC': 'Неправильный ключ.',
        'ERROR_KEY_DESC_TEST': 'Неправильный ключ. Результаты тестирования не сохранены.',
        'ERROR_CATALOG_TITLE': 'Ошибка',
        'ERROR_CATALOG_DESC': 'Внимание, система обнаружила повреждения в базе данных. Вам необходимо синхронизировать данные.',
        'SYSTEM_ORDER_SUB': 'Система заказов',
        'SYSTEM_ORDER_SUB_NOW_LIST_ORDER': 'Заказы на исполнение',
        'SYSTEM_ORDER_SUB_HISTORY_ORDER': 'Архив заказов',
        'SYSTEM_ORDER_SUB_CONNECT_SERVER': 'Подключиться к серверу',
        'SYSTEM_ORDER_SUB_SAVE_BIDS': 'Сохранить заявки на устройство',
        'SYSTEM_ORDER_SUB_HD_ORDER': 'Все заказы',
        'SYSTEM_ORDER_TITLE': 'Система заказов',
        'FORGOTPASS_TITLE': 'Восстановление пароля',
        'FORGOTPASS_MESS_ERROR': 'Заполните поле логин и нажмите "Напомнить пароль".',
        'FORGOTPASS_MESS': 'На ваш адрес электронной почты отправлен пароль для входа.',
        'SUPPORT_TITLE': 'Техническая поддержка',
        'SUPPORT_MESS': 'Техническая поддержка осуществляется по электронному адресу support@rb.librarycrm.com.'

    };

    App.stringLocal['EN'] = {
        'TESTIROVANIE': 'Testing',
        'NOVINKI': 'New',
        'LICHNYY-KABINET': 'Personal<br/>page',
        'TRENINGI': 'Trainings',
        'INSTRUMENTY-PRODAZH': 'Sales<br/>tools',
        'INFORMATSIYA-PO-BRENDAM': 'Informations brands',
        'DETEYLING': 'Detailing',
        'SEND': 'Send',
        'FILES': 'Files',
        'CHAT': 'Chat',
        'FAQ': 'FAQ',
        'FILES_UPLOAD': 'Uploaded files',
        'FROM': 'of',
        'CHECK_UPLOAD': 'Check for updates',
        'CATALOG_ACTUAL': 'Catalog is up to date',
        'ERROR_INTERNET': 'Error Internet connection.',
        'ERROR_SEND': 'Error while sending the message. Check your Internet connection and try again.',
        'ERROR_AUTH': 'A user with this e-mail or password not found!',
        'ERROR': 'Error',
        'CHANGE_CATEGORY': 'Select a category',
        'FILL_IN_ALL_FIELDS': 'Fill in all fields',
        'ERROR_EMAIL': 'Incorrect email',
        'MES_GOOD_REGISTRATION': 'Thank you for signing up. In the near future you will come to the post office password to log in.',
        'REGISTRATION': 'Check In',
        'SEND_GOOD_REQUEST': 'The data has been saved successfully.',
        'SEND_TITLE_ANKET': 'Sending questionnaires to the server',
        'ERROR_ANKET_REQUEST': 'Error. Perhaps no interent connection, the data will be sent after.',
        'FILES': 'Files',
        'CHAT': 'Chat',
        'ALL': 'All',
        'LOGIN': 'Login',
        'CHECK_IN': 'Check In',
        'SKU': 'SKU',
        'REQUEST_CHANGE_CATALOG': 'Attention, catalog needs updating. Request a check for updates?',
        'PAGE': 'Page',
        'SEARCH': 'Search',
        'LAST_FILES': 'Last show files',
        'IZBRANNOE': 'Favorites',
        'ERROR_KEY_TITLE': 'Error',
        'ERROR_KEY_DESC': 'Wrong key.',
        'ERROR_KEY_DESC_TEST': 'Wrong key. Test no save.',
        'ERROR_REG_USER': 'Email already registered.',
        'SYSTEM_ORDER_SUB': 'System orders',
        'SYSTEM_ORDER_SUB_NOW_LIST_ORDER': 'Now orders',
        'SYSTEM_ORDER_SUB_HISTORY_ORDER': 'History orders',
        'SYSTEM_ORDER_SUB_CONNECT_SERVER': 'Connect to Server',
        'SYSTEM_ORDER_SUB_SAVE_BIDS': 'Save bids on device',
        'SYSTEM_ORDER_SUB_HD_ORDER': 'All orders',
        'SYSTEM_ORDER_TITLE': 'System order',
        'FORGOTPASS_TITLE': 'Password recovery',
        'FORGOTPASS_MESS_ERROR': 'Fill in your login and click on " Forgot password " .',
        'FORGOTPASS_MESS': 'Password sent in your e-mail.',
        'SUPPORT_TITLE': 'Support',
        'SUPPORT_MESS': 'Email: support@rb.librarycrm.com.'
    };

    function connectWebViewJavascriptBridge(callback) {
        if (window.WebViewJavascriptBridge) {
            callback(WebViewJavascriptBridge)
        } else {
            document.addEventListener('WebViewJavascriptBridgeReady', function () {
                callback(WebViewJavascriptBridge)
            }, false)
        }
    }

    function sendResultTest(stringParam) {
        if (navigator.onLine) {
            $.ajax({
                url: "http://rb.librarycrm.com/save_result_test.php",
                global: false,
                type: "POST",
                data: {
                    STRING: stringParam, KEY: App.key
                },
                dataType: "json",
                success: function (data) {
                    if (data['SAVE'] == 'YES') {
                        showPopup(
                            App.stringLocal[App.langLocal]['SEND_TITLE_ANKET'],
                            App.stringLocal[App.langLocal]['SEND_GOOD_REQUEST']
                         );

                        connectWebViewJavascriptBridge(function (bridge) {
                            bridge.callHandler('clearFileTestResult');
                        });

                    } else if (data['SAVE'] == 'ERROR_KEY') {

                        showPopup(
                            App.stringLocal[App.langLocal]['ERROR_KEY_TITLE'],
                            App.stringLocal[App.langLocal]['ERROR_KEY_DESC_TEST']
                        );

                    } else {

                        showPopup(
                            App.stringLocal[App.langLocal]['SEND_TITLE_ANKET'],
                            App.stringLocal[App.langLocal]['ERROR_ANKET_REQUEST']
                        );

                    }
                }
            });

        } else {

            showPopup(
                App.stringLocal[App.langLocal]['ERROR'],
                App.stringLocal[App.langLocal]['ERROR_INTERNET']
            );
        }
    }

    function showPopup(title, text, callback) {

        callback = callback || function () {return false;}

        var template = '<div class="message-popup light-box"><h2>' + title + '</h2>' +
                '<p>' + text + '</p>' +
                '<div class="button-container h40 pink">' +
                    '<div data-fancybox-close class="button ok popup-ok">Ок</div>' +
                '</div></div>';

        $.fancybox.open({
            src : template,
            type : 'html',
            smallBtn : false,
            width : 300,
            toolbar : false,
            infobar : false,
            arrows : false,
            keyboard : false,
            afterClose: function() {
                callback.call(this);
            }
        });
    }

    function confirmPopup(title, text, callback) {

            var template = '<div class="message-popup  light-box"><h2>' + title + '</h2>' +
                    '<p>' + text + '</p>' +
                    '<div class="button-container h40 pink confirm-button-left">' +
                        '<div data-fancybox-close class="button ok popup-ok">Да</div>' +
                    '</div>' +
                    '<div class="button-container h40 light-gray confirm-button-right">' +
                       '<div data-fancybox-close class="button no popup-no">Нет</div>' +
                    '</div></div>';

            $.fancybox.open({
                src : template,
                type : 'html',
                smallBtn : false,
                width : 300,
                toolbar : false,
                infobar : false,
                arrows : false,
                keyboard : false,
                clickOutside: false,
                afterShow: function() {
                    ret = false;
                    $(".button").on("click", function(event){
                        if($(event.target).is(".ok")){
                            ret = true;
                        } else if ($(event.target).is(".no")){
                            ret = false
                        }
                    });
                },
                afterClose: function() {
                    callback.call(this, ret);
                }
            });
    }

    function removeBg () {
		$('.wrapper').removeClass('bg');
	}

	function unique(arrInner) {
		var obj = {};
		for (var i = 0; i < arrInner.length; i++) {
			obj[arrInner[i]] = true;
		}
		return Object.keys(obj);
	}

	function getFileExtension (typeFile) {

		var linkImg = 'x-file.png';

		if (typeFile === 'type-pdf') {
			linkImg = 'pdf.png';
		} else if (typeFile === 'type-presentation') {
			linkImg = 'presentation.png';
		} else if (typeFile === 'type-test') {
			linkImg = 'tests.png';
		} else if (typeFile === 'type-img') {
			linkImg = 'jpg.png';
		} else if (typeFile === 'type-ppt') {
			linkImg = 'ppt.png';
		} else if (typeFile === 'type-doc') {
			linkImg = 'doc.png';
		} else if (typeFile === 'type-video') {
			linkImg = 'mp4.png';
		} else if (typeFile === 'type-music') {
			linkImg = 'mp3.png';
		} else if (typeFile === 'type-xls') {
			linkImg = 'xls.png';
		} else if (typeFile === 'type-txt') {
			linkImg = 'txt.png';
		}

		return linkImg;

	}

	function in_array(needle, haystack, strict) {

		var found = false, key, strict = !!strict;

		for (key in haystack) {
			if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
				found = true;
				break;
			}
		}

		return found;
	}

	function clickFile() {

		$('.file').on('click', function (e) {
			var url = $(this).attr('href');
			var idFile = $(this).attr('data-id');

			var title = $(this).text();

			App.lastFiles = idFile + ',' + App.lastFiles;

			var addHistory = App.lastFiles.split(',', 100);

			connectWebViewJavascriptBridge(function (bridge) {
				bridge.callHandler('addHistory', addHistory.toString());
			});

			connectWebViewJavascriptBridge(function (bridge) {
				bridge.callHandler('fileClick', {"URL": url, "TITLE": title, "ID": idFile});
			});

			addUsersAction(App.usersActions['OPEN_FILE'], App.userId, idFile, App.typeObj['FILE']);

			e.preventDefault();
		});
	}

	function addUsersAction(typeAction, userId, idObject, typeObj) {

		var dateNow = new Date();
		var data;
		data = {
			'TIME' : dateNow.getDate() + '.' + (dateNow.getMonth() + 1 ) + '.' + dateNow.getFullYear() + ' ' + dateNow.getHours() + ':' + dateNow.getMinutes() + ':' + dateNow.getSeconds(),
			'TYPE' : typeAction,
			'USER' : userId,
			'ID_OBJECT' : idObject,
			'TYPE_OBJ' : typeObj
		};

		connectWebViewJavascriptBridge(function (bridge) {
			bridge.callHandler('addUsersAction', data);
		});
	}

	function сheckFileTestResultJS()
	{
		connectWebViewJavascriptBridge(function (bridge) {
			bridge.callHandler('сheckFileTestResult');
		});
	}

	function dynamicScrollHeight()
	{
		var windowHeight = $(window).outerHeight();
		var headerHeight = $('.header').outerHeight();
		var topLineHeight = $('.top-line').outerHeight() || 0;
		var filterHeight = $('.filter').outerHeight() || 0;
		var assortmentPhotoElement = $('.assortment-photo-container').outerHeight() || 0;
		var scrollSidebarElement = $('.sidebar-scroll');
		var scrollContentElement = $('.content-scroll');
		var scrollMessagesElement = $('.messages-scroll');
		var scrollInstructionElement = $('.instruction-scroll');
		var bottomOffset = 25;
		var topOffset = 25;
		var heightSidebar = windowHeight - headerHeight - topLineHeight - topOffset;
		var heightContent = windowHeight - headerHeight - topLineHeight - filterHeight - assortmentPhotoElement - topOffset - bottomOffset;
		var heightMessages = windowHeight - headerHeight - topLineHeight - topOffset - 1;
		var heightInstruction = windowHeight - headerHeight - topOffset - 3;

		scrollSidebarElement.height(heightSidebar);
		scrollContentElement.height(heightContent);
		scrollMessagesElement.height(heightMessages);
		scrollInstructionElement.height(heightInstruction);
	}

	 function afterViewJob()
	 {
		$('.register-toggle').on('click', function() {
			$('.login-form').hide();
			$('.register-form').show();
		});

		$('.back-link span').on('click', function() {
			$('.login-form').show();
			$('.register-form').hide();
		});

		$('.styled-select-reg').SumoSelect({
			csvDispCount: 0
		});

		$('.styled-select').SumoSelect({
			csvDispCount: 4,
			captionFormat: '{0} пунктов выбрано',
			captionFormatAllSelected: 'Все пункты выбраны!',
			selectAll: true,
			nativeOnDevice: ['BlackBerry', 'iPhone', 'iPad', 'iPod', 'Opera Mini', 'IEMobile', 'Silk'],
			okCancelInMulti: true,
			locale :  ['OK', 'Отмена', 'Выбрать/сбросить все']
		});

		dynamicScrollHeight();

	}
                            
    function initApp() {

        var programArr;

		if (!tryParseJSON(App.jsonStr)) {
			App.damageCatalog = true;
			App.jsonStr = '{"TIMESTAMP_X_UNIX_UPDATE":1419950634,"TIMESTAMP_X_UPDATE":"2014:30:12 18 43 54","UPDATE":"all","APP":"Android"}';
		}

		programArr = JSON.parse(App.jsonStr);

        function tryParseJSON(jsonString)
        {
            try {
                var o = JSON.parse(jsonString);

                if (o && typeof o === "object") {
                    return o;
                }
            }
            catch (e) { }

            return false;
        };

        function showInnerMenu(arr, sectionName)
        {
            if (!!programArr['USERS'][App.userId]) {
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i]['LEVEL'] == '1') {
                        if (arr[i]['ACCESS'].indexOf(programArr['USERS'][App.userId]['TYPE']) == -1) {

                            $('.leftMenu-' + sectionName).append('<a class="button-container h88 leftMenuItem light-gray leftMenuItem_' + sectionName + '">' +
                                                        '<div class="button"><span>' + arr[i][App.langLocal] + '</span></div>' +
                                                     '</a>');
                        } else {

                            var point_new = "";
                            var c = addPointTags(arr[i]['CODE'], sectionName);
                            if (c > 0) {
                                point_new = '<font class="point_new tags-' + arr[i]['CODE'] + ' ">' + c + '</font>';
                            }

                            $('.leftMenu-' + sectionName).append('<a data-filter="' + arr[i]['CODE'] + '" class="button-container h88 leftMenuItem leftMenuItem_' + sectionName + '">' +
                                '<div class="button"><span>' + arr[i][App.langLocal] + '</span></div>' +
                             '</a>');
                        }

                    }
                }
            }
            $('.leftMenu').append('<div class="clr"></div>');
        }

        function showListOfFiles(files, section, innerSection, selFilters, selBrand,  selFiltersTwo)
        {

            $('.files').html('');
            var filesCount = 0;

            for (var i = 0; i < files.length; i++) {
                var showFile = false;

                if (files[i]['ROOT_SECTION_CODE'] === section) {
                    if (in_array(innerSection, files[i]['INNER_SECTION']) || innerSection === 'all') {
                        showFile = true;
                    } else {
                        showFile = false;
                    }
                }

                if (showFile === true) {

                    var point_new = "";
                    if (addPointNewFile(files[i]['FILE']['FILE_NAME'])) {
                        point_new = '<span class="point-notifications"></span>';
                    }

                    $('.files').append('<div class="cell-3">' +
                        '<a data-tags="' + files[i]['FILTER_TAGS'] +
                        '" data-id="' + files[i]['FILE']['ID'] + '" href="' + files[i]['FILE']['FILE_NAME'] +
                        '" class="file filter-result-item" data-file-name="' + files[i]['NAME'] +
                        '" data-icon="' + getFileExtension(files[i]['TYPE_FILE']) + '" >' +
                            '<img src="content-images/' + getFileExtension(files[i]['TYPE_FILE']) +
                            '" alt="" class="file-image" />' +
                            '<div class="file-name">' + files[i]['NAME'] + '</div>' + point_new +
                        '</a>' +
                    '</div>');

                    filesCount += 1;
                }
            }

            if (filesCount === 0) {
                $('.files').append('<div class="empty-section">В разделе отсутсвуют файлы.</div>');
            }

            $('.files').append('<div class="clr"></div>');

			changeFilters();
            clickFile();

        }

        function showLastFiles(files)
        {
            var arrLastFiles = unique(App.lastFiles.split(','));
            var lastFilesString = '';
            for (var j = 0; j < 9; j++) {
                if (arrLastFiles[j] != "" && arrLastFiles[j] !== undefined) {
                    lastFilesString = lastFilesString + ',' + arrLastFiles[j];
                }
            }

            if (!!files) {
                for (var i = 0; i < files.length; i++) {

                    if (lastFilesString.indexOf(files[i]['FILE']['ID']) + 1) {
                        var point_new = "";
                        if (addPointNewFile(files[i]['FILE']['FILE_NAME'])) {
                            point_new = '<span class="point-notifications"></span>';
                        }

                        $('.lastFiles').append('<div class="cell-3">' +
                            '<a data-id="' + files[i]['FILE']['ID'] + '" href="' + files[i]['FILE']['FILE_NAME'] + '" class="file filter-result-item" data-file-name="' + files[i]['NAME'] + '">' +
                                '<img src="content-images/' + getFileExtension(files[i]['TYPE_FILE']) + '" alt="" class="file-image" />' +
                                '<div class="file-name">' + files[i]['NAME'] + '</div>' + point_new +
                            '</a>' +
                        '</div>');

                    }
                }
            }
            $('.lastFiles').append('<div class="clr"></div>');

            clickFile();
        }

        function addPointNewFile(fileName)
        {
            if (App.dataForNewPoint != "" && tryParseJSON(App.dataForNewPoint)) {
                var arr = JSON.parse(App.dataForNewPoint);
                for (var i = 0; i < arr.length; i++) {
                    if (fileName == arr[i]['FILE_NAME']) {
                        return true;
                    }
                }
            }
        }

        function addPointSection(sectionName)
        {
            var c = 0;
            if (App.dataForNewPoint != "" && tryParseJSON(App.dataForNewPoint)) {
                var arr = JSON.parse(App.dataForNewPoint);
                for (var i = 0; i < arr.length; i++) {
                    if (sectionName == arr[i]['ROOT_SECTION_CODE']) {
                        c++;
                    }
                }
                return c;
            }
        }

        function addPointTags(tagsName, sectionName) {
            var b = 0;
            if (App.dataForNewPoint != "" && tryParseJSON(App.dataForNewPoint)) {
                var arr = JSON.parse(App.dataForNewPoint);
                for (var i = 0; i < arr.length; i++) {
                    if (sectionName == arr[i]['ROOT_SECTION_CODE']) {
                        if (in_array(tagsName, arr[i]['SECOND_SECTION_CODE'])) {
                            b++;
                        }
                    }
                }
                return b;
            }
        }

        function showFAQLists(arr) {
            if (!!arr) {
                for (var i = 0; i < arr.length; i++) {
                    $('.faq-list').append('<a class="message filter-result-item" data-file-name="' +
                    arr[i]['NAME'] + '" data-id="' + arr[i]['ID'] + '" data-desc="' + arr[i]['PREVIEW_TEXT'] + '"><p class="message-title">' +
                    arr[i]['NAME'] + '</p><p>' + arr[i]['PREVIEW_TEXT'] + '</p></a>');
                }

                changeFilters();
            }
        }

        function showMessagesLists(arr) {

            var dateNow = new Date();
            dateNow = dateNow.getTime();

            if (!!arr) {
                for (var i = 0; i < arr.length; i++) {

                    var markedClass = '';

                    if (parseInt(arr[i]['DATA_OF_IMPORTANT']) > (dateNow/1000)) {
                       markedClass = 'blue';
                    }

                    $('.messages-list').append('<a class="message ' + markedClass + '"><p class="message-title">' + arr[i]['NAME'] + '</p><p>' + arr[i]['PREVIEW_TEXT'] + '</p></a>');
                }
            }
        }

        function checkActiveMessages (arr) {

            var dateNow = new Date();
            dateNow = dateNow.getTime();

            var result = false;

            if (!!arr) {
                for (var i = 0; i < arr.length; i++) {

                    if (parseInt(arr[i]['DATA_OF_IMPORTANT']) > (dateNow/1000)) {
                       result = true;
                    }

                }
            }
            return result;
        }

        function checkUpdate(str) {

            $('body').append('<div class="mes_check_update fonts-buttom">'
            + App.stringLocal[App.langLocal]['CHECK_UPLOAD'] + '</div>');

            $.ajax({
                url: "http://rb.librarycrm.com/action_mass/check_update.php",
                global: false,
                type: "POST",
                data: {
                    STRING: str, KEY: App.key, APP: App.type_aplication
                },
                dataType: "json",
                success: function (data) {

                    var mes = "";

                    if (data['STATUS'] == 'NOUPDATE') {

                        mes = App.stringLocal[App.langLocal]['CATALOG_ACTUAL'];

                    } else if (data['STATUS'] == 'ERROR_KEY') {

                        showPopup(
                            App.stringLocal[App.langLocal]['ERROR_KEY_TITLE'],
                            App.stringLocal[App.langLocal]['ERROR_KEY_DESC']
                        );

                    } else {

                        var messageForFirstSync = "";
						var message = "";

						if (programArr['FILES'] === undefined) {
							messageForFirstSync = "Внимание, в приложение нет файлов. "
						}

						if (App.damageCatalog) {
							message = App.stringLocal[App.langLocal]['ERROR_CATALOG_DESC'];
						} else if (parseInt(data['FILES_COUNT']) > 1) {
					  		message = messageForFirstSync + "Загрузить " + data['FILES_COUNT']
					  		+ " файлов размером " + data['FILES_SIZE'] + "&nbsp;Мб.?";
						} else {
						  message = "Загрузить обновлённые данные?";
						}

						confirmPopup("Загрузка обновлений",
							message,
							function(resp) {
							if (typeof ret !== "undefined") {
								ret = false;
							}

							if (resp) {
								App.dataUpdate = data;
								connectWebViewJavascriptBridge(function (bridge) {
									bridge.callHandler('SyncDB', data);
								});
							}
						});

                    }
                    if (mes == "") {
                        $('.mes_check_update').html('<a class="request-check-update">'
                        + App.stringLocal[App.langLocal]['REQUEST_CHANGE_CATALOG'] + '</a>');
                    } else {
                        $('.mes_check_update').html(mes);
                        setTimeout(function() {
                            $('.mes_check_update').hide();
                        }, 10 * 1000);

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('.mes_check_update').html('Устройство не подключено к интернету. Работа продолжится в оффлайн режиме.');
                    setTimeout(function() {
                        $('.mes_check_update').hide();
                    }, 10 * 1000);
                }
            });

        }

        function goCheckUpdate() {
            if (!App.updateProcess) {
                if (!App.lastUpdate) {
                    checkUpdate(App.jsonStr);
                    App.lastUpdate = Date.now();

                } else if (Date.now() - App.lastUpdate >= 60 * 60 * 1000) {
                    checkUpdate(App.jsonStr);
                    App.lastUpdate = Date.now();
                }
            }
            setTimeout(goCheckUpdate, 5 * 60 * 1000);
        }

        function showFilter(name, styleClass) {

            var options = [];
            var optionsHtml = '';

            if (name === 'brands') {
                options = programArr['FILTERS']['BRANDS'];
            } else if (name === 'net') {
                options = programArr['FILTERS']['NET'];
            } else if (name === 'format-net') {
                options = programArr['FILTERS']['FORMAT_NET'];
            } else if (name === 'material-type') {
                options = programArr['FILTERS']['MATERIAL_TYPE'];
            } else if (name === 'month') {
                options = programArr['FILTERS']['MONTH'];
            }

            for (var i = 0; i < options.length; i++) {
                optionsHtml = optionsHtml + '<option value="' + options[i]['CODE'] +
                '" >' + options[i]['NAME'] + '</option>';
            }

            $('.' + styleClass).html(optionsHtml);

        }

        function showFilterMulti(name, styleClass, section, sectionInner) {

            var options = [];
            var optionsHtml = '';

            if (name === 'net') {
                options = programArr['FILTERS']['NET'];
            }

            for (var i = 0; i < options.length; i++) {
                if (options[i]['SECTION'] === section && options[i]['SECTION_INNER'] === sectionInner) {
                    optionsHtml = optionsHtml + '<option value="' + options[i]['CODE'] +
                    '" >' + options[i]['NAME'] + '</option>';
                }
            }

            $('.' + styleClass).html(optionsHtml);

        }

        function changeFilters() {
            var searchName = '';
            var allFiles = [];
            var selectedTags = [];
            var filteredFiles = [];
            var $root = $(".filter-result-area");
            var $allFilesElements = $(".filter-result-item");
            var cellType = $root.attr('data-cell-type');

            function getTemplate(file){

                var point_new = "";
                if (addPointNewFile(file.fileLink)) {
                    point_new = '<span class="point-notifications"></span>';
                }

                var template = '<div class="' + cellType + '">' +
                                    '<a data-tags="' + file.fileTags.join('|') +
								 	' data-icon="' + file.fileIcon +
								 	'" data-id="' + file.fileId +
                                    '" href="' + file.fileLink +
                                    '" data-file-name="' + file.fileName +
                                    '" class="file filter-result-item" >' +
                                        '<img src="content-images/' + file.fileIcon +
                                        '" alt="" class="file-image" />' +
                                        '<div class="file-name">' + file.fileName + '</div>' + point_new
                                    '</a>' +
                                '</div>';

              	return template;
            }

            function getTemplateFaq(item){

				var template = '<a class="message filter-result-item" data-file-name="' +
                                    item.fileName + '" data-id="' + item.fileId + '" data-desc="' +
                                    item.fileDesc + '"><p class="message-title">' +
                                    item.fileName + '</p><p>' + item.fileDesc + '</p></a>';

			  	return template;
			}

            function updateRenderFiles(type){
				$root.html('');

				var renderArray = [];
				var files = searchName === '' && selectedTags.length === 0 ? allFiles : filteredFiles;

				files.forEach(function(item) {
					if (type == 'faq') {
						renderArray.push(getTemplateFaq(item));
					} else {
						renderArray.push(getTemplate(item));
					}
				});

				if(renderArray.length > 0) {
					$root.append(renderArray);
				}
			}

			function getSelectsValue(){
				selectedTags = [];
				var selects = $('.styled-select');
				$.each(selects, function(index, select) {
					var value = $(select).val();
					selectedTags = selectedTags.concat(value);
				});
			}

            function addFileToFilteredFiles(newFile){
				var isAlready = filteredFiles.filter(function(file) {
					return file.fileId === newFile.fileId;
				});

				if (!isAlready.length){
					filteredFiles.push(newFile);
				}
            }

            function emptyFilteredFiles(){
				filteredFiles = [];
            }

            function filterByName(files){
				if(searchName !== ''){
					var tmpFiles = files.length > 0 ? files : allFiles;
					emptyFilteredFiles();
					tmpFiles.forEach(function(file) {
						if(file.fileName.toUpperCase().indexOf(searchName.toUpperCase()) > -1) {
							addFileToFilteredFiles(file);
						}
					});
				}
            }

            function filterBySelect(){
				if(selectedTags.length > 0){
					emptyFilteredFiles();

					for (var i = 0; i < selectedTags.length; i++) {
						for (var j = 0; j < allFiles.length; j++) {
							for (var w = 0; w < allFiles[j].fileTags.length; w++) {
								if(selectedTags[i].toUpperCase() === allFiles[j].fileTags[w].toUpperCase()){
									addFileToFilteredFiles(allFiles[j]);
								}
							}
						}
					}
				}
            }

            function mainFilter() {
				filterBySelect();
				filterByName(filteredFiles);
				updateRenderFiles('files');
            }

            function faqFilter() {
				filterByName(filteredFiles);
				updateRenderFiles('faq');
            }


			$.each($allFilesElements, function( index, value ) {
				var fileName = $(value).attr('data-file-name');
				var fileLink = $(value).attr('href');
				var fileTags = $(value).attr('data-tags');
				var fileId = $(value).attr('data-id');
				var fileIcon = $(value).attr('data-icon');
				var fileDesc = $(value).attr('data-desc');
				fileTags = fileTags ? fileTags.split('|') : [];
				allFiles.push({ fileName: fileName, fileLink: fileLink, fileTags: fileTags, fileId: fileId, fileIcon: fileIcon, fileDesc: fileDesc});
			});

			$('.search input').on("keyup",function() {
				searchName = this.value;

				if (!!$(this).attr('data-type')) {
					faqFilter();
				} else {

					mainFilter();
				}
			});

			$('.search input').on("blur",function() {
				searchName = this.value;

				if (!!$(this).attr('data-type')) {
					faqFilter();
				} else {

					mainFilter();
				}
			});

			$('.filter-change').on("change",function() {
				getSelectsValue();
				mainFilter();
				clickFile();
			});
        }


        $("body").on('click', '.mes_check_update .request-check-update', function () {
            checkUpdate(App.jsonStr);
        });

        goCheckUpdate();
        setTimeout(goCheckUpdate, 5000);

        var Controller = Backbone.Router.extend({
            routes: {
                "": "login",
                "!/": "login",
                "!/main": "main",
                "!/mslSection": "mslSection",
                "!/swbSection": "swbSection",
                "!/mPrioritiesSection": "mPrioritiesSection",
                "!/libraryHealthSection": "libraryHealthSection",
                "!/stepsOfVisitSection": "stepsOfVisitSection",
                "!/newsSection": "newsSection",
                "!/testsSection": "testsSection",
                "!/faqSection": "faqSection",
                "!/messagesSection": "messagesSection",
                "!/brandsSection": "brandsSection",
                "!/historySection": "historySection",
                "!/testirovanie": "testirovanie",
            },

            login: function () {
                if (Views.authorizeView != null) {
                    Views.authorizeView.render();
                    afterViewJob();
                }
            },
            main: function () {
                if (Views.mainView != null) {
                    Views.mainView.render();

                }
            },
            mslSection: function () {
                if (Views.mslSectionView != null) {
                    Views.mslSectionView.render();
                    showListOfFiles(programArr['FILES'], 'msl', 'all');
                    showFilter('format-net', 'filter-material');
                    afterViewJob();
                }
            },
            swbSection: function () {
                if (Views.swbSectionView != null) {
                    Views.swbSectionView.render();
                    afterViewJob();
                }
            },
            mPrioritiesSection: function () {
                if (Views.mPrioritiesSectionView != null) {
                    Views.mPrioritiesSectionView.render();
                    afterViewJob();
                }
            },
            libraryHealthSection: function () {
				if (Views.libraryHealthSectionView != null) {
					Views.libraryHealthSectionView.render();
					showListOfFiles(programArr['FILES'], 'libraryHealth', 'all');
					afterViewJob();
				}
			},
            stepsOfVisitSection: function () {
                if (Views.stepsOfVisitSectionView != null) {
                    Views.stepsOfVisitSectionView.render();
                    showListOfFiles(programArr['FILES'], 'stepsOfVisit', 'all');
                    afterViewJob();
                }
            },
            newsSection: function () {
                if (Views.newsSectionView != null) {
                    Views.newsSectionView.render();
                    afterViewJob();
                }
            },
            testsSection: function () {
                if (Views.testsSectionView != null) {
                    Views.testsSectionView.render();

                    afterViewJob();
                }
            },
            faqSection: function () {
                if (Views.faqSectionView != null) {
                    Views.faqSectionView.render();
                    showFAQLists(programArr['FAQ']);
                    afterViewJob();
                }
            },
            messagesSection: function () {
                if (Views.messagesSectionView != null) {
                    Views.messagesSectionView.render();
                    showMessagesLists(programArr['MESSAGES']);
                    afterViewJob();
                }
            },
            brandsSection: function () {
                if (Views.brandsSectionView != null) {
                    Views.brandsSectionView.render();
                    showListOfFiles(programArr['FILES'], 'brands', 'all');
                    showFilter('brands', 'filter-brands');
                    showFilter('material-type', 'filter-material');
                    afterViewJob();
                }
            },
            historySection: function () {
                if (Views.historySectionView != null) {
                    Views.historySectionView.render();
                    showLastFiles(programArr['FILES']);

                    afterViewJob();
                }
            },
            testirovanie: function () {
                if (Views.testirovanieView != null) {
                    Views.testirovanieView.render();
                    $('#header').css({display: 'block'});

                }
            }
        });

        var controller = new Controller();

        var AuthorizeView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#login_' + App.langLocal.toLowerCase()).html()),

            events: {
                "click input.login-auth-sub:submit": "login",
                "click input.registration-sub:submit": "register",
                "click .forgotpass": "forgotpass",
                "click .support": "support"
            },

            login: function () {
                for (var key in programArr['USERS']) {
                    var name = $('input[name="login"]').val();
                    var pass = $('input[name="pass"]').val();
                    if (programArr['USERS'][key]['EMAIL'] == name.toLowerCase() && programArr['USERS'][key]['PASS_NO_CODE'] == pass) {

                        App.user = name;
                        App.userPass = pass;
                        App.userId = programArr['USERS'][key]['ID'];
                        break;
                    }
                }

                if (programArr['FILES'] === undefined) {
                    showPopup('Авторизация', 'Для первой работы с приложением, пожалуйста загрузите обновления.');
                } else if (!App.user) {
                    showPopup('Авторизация', 'Неверный логин или пароль.');
                    return false;
                } else {
                    var mes = {"LOGIN": App.user, "PASS": App.userPass, "ID": App.userId};

                    controller.navigate("!/main", {trigger: true, replace: true});
                }
            },

            register: function () {
                var submit = $('.registration-sub');

                $('.register-button div').addClass('light-gray').removeClass('pink');
                submit.attr("disabled", true);

                function activeRegSub () {
                    submit.attr("disabled", false);
                    $('.register-button div').addClass('pink').removeClass('light-gray');
                }

                (event.preventDefault) ? event.preventDefault() : event.returnValue = false;

                var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
                var error_email = false;
                var error_empty = false;
                var email = $('[name="email"]').val();
                if (email.search(emailRegEx) == -1) { error_email = true; }

                $('.req_field').each(function () {
                    if ($(this).val() == '') {
                        error_empty = true;
                    }
                });

                if (error_email === false && error_empty === false) {
                    $.post('http://rb.webgrate.ru/action_mass/reg_user.php',
                    	{data: $('#reg_form').serializeArray()},
                    	function (data) {
							if (data.success) {

								showPopup(
									App.stringLocal[App.langLocal]['REGISTRATION'],
									App.stringLocal[App.langLocal]['MES_GOOD_REGISTRATION']
								);

								App.user = email;

								controller.navigate("!/main", true);

							} else {
								if (data.err == 1) {
									showPopup(
										App.stringLocal[App.langLocal]['REGISTRATION'],
										App.stringLocal[App.langLocal]['ERROR_REG_USER'],
										activeRegSub
									);
								} else {
									showPopup(
										App.stringLocal[App.langLocal]['REGISTRATION'],
										App.stringLocal[App.langLocal]['ERROR'],
										activeRegSub
									);
								}
							}
                    }, 'json')
                    .fail(function() {
                        showPopup(
                            App.stringLocal[App.langLocal]['REGISTRATION'],
                            'Устройство не подключено к интернету. Для регистрации необходим доступ к сети.',
                            activeRegSub
                        );
                        return false;
                    });
                } else {
                    var alertsMess = '';

                    $('.req_field').each(function () {
                        if ($(this).val() == '') {
                            alertsMess = alertsMess + ' &mdash; ' + $(this).attr('data-title') + '<br/>';
                        }
                    });

                    if (alertsMess == '') {
                        alertsMess ='Пожалуйста, заполните все поля корректно.';
                    } else {
                        alertsMess = '<p align="left">Вам необходимо заполнить:<br/>' + alertsMess + '</p>';
                    }

                    showPopup(
                        App.stringLocal[App.langLocal]['REGISTRATION'],
                        alertsMess,
                        activeRegSub
                    );
                }

                return false;
            },

            forgotpass: function () {
                var name = $('input[name="login"]').val();
                if (name != '') {
                    var data = {"USER": name, "KEY": App.key, "LANG": App.langLocal};

                    $.post('http://rb.webgrate.ru/action_mass/forgotpass.php', data, function (data) {
                        showPopup(
                            App.stringLocal[App.langLocal]['FORGOTPASS_TITLE'],
                            data.mess
                        );
                    }, 'json');

                } else {
                    showPopup(
                        App.stringLocal[App.langLocal]['FORGOTPASS_TITLE'],
                        App.stringLocal[App.langLocal]['FORGOTPASS_MESS_ERROR']
                    );
                }
            },

            support: function () {
                showPopup(
                    App.stringLocal[App.langLocal]['SUPPORT_TITLE'],
                    App.stringLocal[App.langLocal]['SUPPORT_MESS']
                );
            },

            render: function () {
                $(this.el).html(this.template());
                return this;
            },
        });
        var MainView = Backbone.View.extend({
            el: $("#content"),
            template: _.template($('#mainPage').html()),
            events: {

            },
            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());

                setTimeout(function() {

                }, 5000);

                mainSectionsAccess = [
                    ['msl', 'ho,ums,asms'],
                    ['swb', 'ho,ums,asms,exmerch,kasrs,dsrs'],
                    ['mPriorities', 'ho,ums,asms,exmerch,kasrs,dsrs'],
                    ['libraryHealth', 'ho,exmerch'],
                    ['stepsOfVisit', 'ho,ums,asms,exmerch,kasrs,dsrs'],
                    ['news', 'ho,ums,asms,exmerch,kasrs,dsrs'],
                    ['brands', 'ho,ums,asms'],
                ];

                if (!!programArr['USERS'] && !!programArr['USERS'][App.userId] && !!programArr['USERS'][App.userId]['TYPE']) {

                    for (var i = 0; i < mainSectionsAccess.length; i++) {

                        if ( programArr['USERS'][App.userId]['TYPE'] != 'undefined' && mainSectionsAccess[i][1].indexOf(programArr['USERS'][App.userId]['TYPE']) != -1) {
                            $('#' + mainSectionsAccess[i][0] + 'Link').attr('href', '#!/' + mainSectionsAccess[i][0] + 'Section');
                            $('#' + mainSectionsAccess[i][0] + 'Link').removeClass('light-gray');

                            var point_new = "";
                            var c = addPointSection(mainSectionsAccess[i][0]);
                            if (c > 0) {
                                point_new = '<div class="count-notifications"><span>' + c + '</span></div>';
                                $('#' + mainSectionsAccess[i][0] + 'Link .notification').html(point_new);
                            }

                        }

                    }
                } else {
                    $('.attention-block').show().html('Нет данных о пользователе. Необходимо принять обновления каталога.');
                }
                return this;
            },
        });
        var MslSectionView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#mslSection').html()),

            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());
                return this;
            },
        });
        var SwbSectionView = Backbone.View.extend({
            el: $("#content"),
            template: _.template($('#swbSection').html()),
            events: {
                "click .leftMenuItem_swb": "showFilesInSection"
            },
            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());
                showInnerMenu(programArr['INNER_STRUCT']['swb'], 'swb');
                return this;
            },
            showFilesInSection: function (el) {
                var innerSection = $(el.currentTarget).data('filter');
                $('.leftMenuItem.active').removeClass('active');
                $(el.currentTarget).addClass('active');
                showListOfFiles(programArr['FILES'], 'swb', innerSection);
                showFilter('format-net', 'filter-net-format');
                showFilterMulti('net', 'filter-net', 'swb', innerSection);

                $('.filter-net')[0].sumo.reload();
                $('.filter-net-format')[0].sumo.reload();
            }
        });
        var MPrioritiesSectionView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#mPrioritiesSection').html()),
            events: {
                "click .leftMenuItem_mPriorities": "showFilesInSection"
            },
            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());

                showInnerMenu(programArr['INNER_STRUCT']['mPriorities'], 'mPriorities');
                return this;
            },
            showFilesInSection: function (el) {
                var innerSection = $(el.currentTarget).data('filter');
                $('.leftMenuItem.active').removeClass('active');
                $(el.currentTarget).addClass('active');
                showListOfFiles(programArr['FILES'], 'mPriorities', innerSection);
                showFilter('format-net', 'filter-net-format');
                showFilterMulti('net', 'filter-net', 'mPriorities', innerSection);
                showFilter('month', 'filter-month', 'mPriorities', innerSection);

                $('.filter-net')[0].sumo.reload();
                $('.filter-net-format')[0].sumo.reload();
                $('.filter-month')[0].sumo.reload();
            }
        });
        var LibraryHealthSectionView = Backbone.View.extend({
			el: $("#content"),

			template: _.template($('#libraryHealthSection').html()),

			render: function () {
				removeBg();
				Views.headerView.render();
				$(this.el).html('');
				$(this.el).html(this.template());
				return this;
			},
		});
        var StepsOfVisitSectionView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#stepsOfVisitSection').html()),

            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());
                return this;
            },
        });
        var NewsSectionView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#newsSection').html()),
            events: {
                "click .leftMenuItem_news": "showFilesInSection"
            },
            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());

                showInnerMenu(programArr['INNER_STRUCT']['news'], 'news');

                return this;
            },
            showFilesInSection: function (el) {
                var innerSection = $(el.currentTarget).data('filter');
                $('.leftMenuItem.active').removeClass('active');
                $(el.currentTarget).addClass('active');
                showListOfFiles(programArr['FILES'], 'news', innerSection);
            }
        });
        var TestsSectionView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#testsSection').html()),
            events: {
                "click .leftMenuItem_tests": "showFilesInSection"
            },
            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());

                if (!!programArr['INNER_STRUCT'] && programArr['INNER_STRUCT']['tests']) {
                    showInnerMenu(programArr['INNER_STRUCT']['tests'], 'tests');
                }
                return this;
            },
            showFilesInSection: function (el) {
                var innerSection = $(el.currentTarget).data('filter');
                $('.leftMenuItem.active').removeClass('active');
                $(el.currentTarget).addClass('active');
                showListOfFiles(programArr['FILES'], 'tests', innerSection);
            }
        });
        var FaqSectionView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#faqSection').html()),

            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());
                return this;
            }
        });
        var MessagesSectionView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#messagesSection').html()),

            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());
                return this;
            },
        });
        var BrandsSectionView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#brandsSection').html()),

            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());
                return this;
            },
        });
        var HistorySectionView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#historySection').html()),

            render: function () {
                removeBg();
                Views.headerView.render();
                $(this.el).html('');
                $(this.el).html(this.template());
                return this;
            },
        });
        var TestirovanieView = Backbone.View.extend({
            el: $("#content"),

            template: _.template($('#pageb').html()),

            render: function () {
                $(this.el).html('');
                $(this.el).html(this.template());
                $(this.el).append('<div class="top-content"></div>');
                $(this.el).append('<div class="wrapp-list-file"></div>');
                return this;
            },
        });
        var HeaderView = Backbone.View.extend({
            el: $("#header"),

            template: _.template($('#headerTemplate').html()),
            events: {
                "click .exit": "exit",
                "click .notification": "notificationShow",
                "click .settings": "settingsShow"
            },
            render: function () {
                $(this.el).html('');
                $(this.el).html(this.template());

                $('.user-name').html(App.user);

                if (checkActiveMessages(programArr['MESSAGES'])) {
                    $('.notification').html('<div class="count-notifications">!</div>');
                }

                return this;
            },
            exit: function () {
                connectWebViewJavascriptBridge(function (bridge) {
                    bridge.callHandler('userExit');
                });
            },
            notificationShow: function () {
                controller.navigate("!/messagesSection", true);
            },
            settingsShow: function () {
               connectWebViewJavascriptBridge(function (bridge) {
                   bridge.callHandler('settingsShow');
               });
            }
        });


        Views = {
            authorizeView: new AuthorizeView(),
            mainView: new MainView(),
            mslSectionView: new MslSectionView(),
            swbSectionView: new SwbSectionView(),
            mPrioritiesSectionView: new MPrioritiesSectionView(),
            libraryHealthSectionView: new LibraryHealthSectionView(),
            stepsOfVisitSectionView: new StepsOfVisitSectionView(),
            newsSectionView: new NewsSectionView(),
            testsSectionView: new TestsSectionView(),
            faqSectionView: new FaqSectionView(),
            messagesSectionView: new MessagesSectionView(),
            brandsSectionView: new BrandsSectionView(),
            historySectionView: new HistorySectionView(),
            headerView: new HeaderView(),
            testirovanieView: new TestirovanieView()

        };

        Backbone.history.start();
    }

    connectWebViewJavascriptBridge(function (bridge) {

            bridge.init(function (message, responseCallback) {

                var messageJson = JSON.parse(message);

                if (messageJson['UPDATE']) {
                    if (messageJson['UPDATE'] == "GO") {
                        var countUploadFiles = App.dataUpdate["UPDATE_DATA"]["FILES"].length;
                        $('.mes_check_update').show();
                        $('.mes_check_update').html(App.stringLocal[App.langLocal]['FILES_UPLOAD'] + " <span class='count_upload_files'>0</span> " + App.stringLocal[App.langLocal]['FROM'] + ' ' + countUploadFiles);
                        App.updateProcess = true;
                    }
                    else if (messageJson['UPDATE'] == "GO_ONE") {
                        var countUploadFiles = 1

                        $('.mes_check_update').show();
                        App.stringMessageStatus = $('.mes_check_update').html();
                        $('.mes_check_update').html(App.stringLocal[App.langLocal]['FILES_UPLOAD'] + " <span class='count_upload_files'>0</span> " + App.stringLocal[App.langLocal]['FROM'] + ' ' + countUploadFiles);
                        App.updateProcess = true;
                    } else if (messageJson['UPDATE'] == "GO_ONE_END") {
                        $('.mes_check_update').html(App.stringMessageStatus);
                        App.updateProcess = false;
                    } else {
                        $('.mes_check_update').html('<a class="request-check-update">' + App.stringLocal[App.langLocal]['REQUEST_CHANGE_CATALOG'] + '</a>');
                        App.updateProcess = false;
                    }
                }
                if (messageJson['COUNT_UPDATE_FILES']) {
                    $('.mes_check_update .count_upload_files').text(messageJson['COUNT_UPDATE_FILES']);
                }
                if (messageJson['USER_LANG']) {
                    var langLocalFull = messageJson['USER_LANG'].toUpperCase();
                    if (langLocalFull.indexOf('EN') != -1) {
                        App.langLocal = 'EN';
                    } else if (langLocalFull.indexOf('RU') != -1) {
                        App.langLocal = 'RU';
                    } else {
                        App.langLocal = 'RU';
                    }

                }
                if (messageJson['JSON_STRING_USER']) {
                    var mes = JSON.parse(messageJson['JSON_STRING_USER']);

                    App.user = mes['LOGIN'];
                    App.userPass = mes['PASS']
                    App.userId = mes['ID'];
                }
                if (messageJson['UPDATE_FILE']) {
                    App.dataForNewPoint = messageJson['UPDATE_FILE'];
                }
                if (messageJson['JSON_STRING']) {
                    App.jsonStr = messageJson['JSON_STRING'];
                    initApp();
                }
                if (messageJson['DATA_FROM_TESTRESULT']) {
                    sendResultTest(messageJson['DATA_FROM_TESTRESULT']);
                }
                if (message['STRING_IZBRANNOE']) {
                    App.izbrannoeFiles = message['STRING_IZBRANNOE'];
                }
                if (messageJson['STRING_HISTORY']) {
                    App.lastFiles = messageJson['STRING_HISTORY'];

                }
                if (message['STRING_LIST_BIDS']) {
                    App.listBids = message['STRING_LIST_BIDS'];
                }

            })

        });

    window.addEventListener("resize", function() {
		$(".wrapper").css("min-height", "528px");
		afterViewJob();
	}, false);

	$(".wrapper").css("min-height", "528px");

	setTimeout(сheckFileTestResultJS, 3 * 1000);
})();