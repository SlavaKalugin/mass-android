package ru.brandspot.Mass;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;


import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;


public class PdfViewerActivity extends AppCompatActivity {
    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfviewer);

        Intent intent = getIntent();

        Toolbar topToolbar = (Toolbar) findViewById(R.id.toolbar);
        topToolbar.setTitle(intent.getStringExtra("fileTitle"));
        setSupportActionBar(topToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        topToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (intent.getStringExtra("fileName").equals("template/welcome_local.pdf")) {
            pdfView = (PDFView) findViewById(R.id.pdfView);
            pdfView.fromAsset(intent.getStringExtra("fileName")).load();
        } else {
            File filePdf = new File(getFilesDir(), intent.getStringExtra("fileName"));
            pdfView = (PDFView) findViewById(R.id.pdfView);
            pdfView.fromFile(filePdf).load();
        }
    }
}
