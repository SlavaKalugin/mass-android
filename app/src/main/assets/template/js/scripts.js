$(document).ready(function() {
  $('.register-toggle').on('click', function() {
    $('.login-form').hide();
    $('.register-form').show();
  });

  $('.back-link span').on('click', function() {
    $('.login-form').show();
    $('.register-form').hide();
  });

  $('.styled-select').SumoSelect({csvDispCount: 0, selectAll: true});


  dynamicScrollHeight();



  $('.light-box .ok').on('click', function() {
    $.fancybox.close( true );
  });

  $('.password-reminder').on('click', function() {
    var input = $('.login-form input.login');
    if(input.val().length <= 0)
      $.fancybox.open($('#password-recovery-light-box'));
  });

  $('[data-fancybox]').fancybox({
    smallBtn : false,
    toolbar  : false,
  });

});

$(window).on('load', function() {
  $('.mCustomScrollbar').mCustomScrollbar({
    theme: 'minimal',
    autoHideScrollbar: false,
    advanced: { updateOnContentResize: true },
  });

  $('.SumoSelect > .optWrapper ul').mCustomScrollbar({
    theme: 'minimal',
    autoHideScrollbar: false,
    advanced: { updateOnContentResize: true },
  });
});

$(window).on('resize', function() {
  dynamicScrollHeight();
});

function dynamicScrollHeight(){
  var windowHeight = $(window).outerHeight();
  var headerHeight = $('.header').outerHeight();
  var topLineHeight = $('.top-line').outerHeight() || 0;
  var filterHeight = $('.filter').outerHeight() || 0;
  var assortmentPhotoElement = $('.assortment-photo-container').outerHeight() || 0;
  var scrollSidebarElement = $('.sidebar-scroll');
  var scrollContentElement = $('.content-scroll');
  var scrollMessagesElement = $('.messages-scroll');
  var scrollInstructionElement = $('.instruction-scroll');
  var bottomOffset = 25;
  var topOffset = 25;
  var heightSidebar = windowHeight - headerHeight - topLineHeight - topOffset;
  var heightContent = windowHeight - headerHeight - topLineHeight - filterHeight - assortmentPhotoElement - topOffset - bottomOffset;
  var heightMessages = windowHeight - headerHeight - topLineHeight - topOffset - 1;
  var heightInstruction = windowHeight - headerHeight - topOffset - 3;

  scrollSidebarElement.height(heightSidebar);
  scrollContentElement.height(heightContent);
  scrollMessagesElement.height(heightMessages);
  scrollInstructionElement.height(heightInstruction);
}
