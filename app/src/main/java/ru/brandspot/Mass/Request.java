package ru.brandspot.Mass;

/**
 * Created by kaluginslava on 3/20/18.
 */

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Request {
	@FormUrlEncoded
	@POST("/action_mass/save_stat.php")
	Call<Object> performPostCall(@FieldMap HashMap<String, String> postDataParams);
}
