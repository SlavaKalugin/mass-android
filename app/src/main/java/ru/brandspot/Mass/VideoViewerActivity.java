package ru.brandspot.Mass;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.File;


public class VideoViewerActivity extends AppCompatActivity {

    VideoView videoPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoviewer);

        Intent intent = getIntent();

        Toolbar topToolbar = (Toolbar) findViewById(R.id.toolbar);
        topToolbar.setTitle(intent.getStringExtra("fileTitle"));
        setSupportActionBar(topToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        topToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        File file = new File(getFilesDir(), intent.getStringExtra("fileName"));
        String filePath = file.getPath();

        videoPlayer =  (VideoView)findViewById(R.id.videoPlayer);
        videoPlayer.setVideoPath(filePath);
        MediaController mediaController = new MediaController(this);
        videoPlayer.setMediaController(mediaController);
        mediaController.setMediaPlayer(videoPlayer);
        videoPlayer.start();
    }

}
