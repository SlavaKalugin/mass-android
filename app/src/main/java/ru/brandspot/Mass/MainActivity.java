package ru.brandspot.Mass;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.widget.Toast;

import com.github.lzyzsd.jsbridge.BridgeHandler;
import com.github.lzyzsd.jsbridge.BridgeWebView;
import com.github.lzyzsd.jsbridge.CallBackFunction;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.ThinDownloadManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION;
import static android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
import static android.support.v4.content.FileProvider.getUriForFile;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_APP = "LOG_APP";
    private String answerHTTP;

    private final String server = "http://rb.librarycrm.com";

    private Gson gson = new GsonBuilder().create();

    private Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(server)
            .build();

    private Request req = retrofit.create(Request.class);

    public void sendStat() throws IOException {
        try {
            String usersActionsData = readFile("usersActions_file_data.txt");

            HashMap<String, String> postDataParams = new HashMap<String, String>();
            postDataParams.put("STAT", usersActionsData);
            postDataParams.put("KEY", "Wert4Df_n1igQ");

            Call<Object> call = req.performPostCall(postDataParams);

            call.enqueue(new Callback<Object>() {
                @Override
                public void onResponse(Call<Object> call, Response<Object> response) {
                    HashMap<String, String> map =
                            gson.fromJson(response.body().toString(), HashMap.class);

                    answerHTTP = map.get("SAVE");

                    if (answerHTTP.toLowerCase().equals("yes")) {
                        try {
                            overWriteFile("usersActions_file_data.txt", "");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<Object> call, Throwable t) {
                    Log.d(LOG_APP, "Request error");
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void copyBaseFile(String fileName, String copyFileName) throws IOException {

        File fileJson = new File(getFilesDir(), fileName);
        if (!fileJson.exists()) {

            try {
                FileOutputStream fileJsonWrite = openFileOutput(fileName, MODE_PRIVATE);
                fileJsonWrite.write(MainActivity.readAssestsFile(getAssets(), copyFileName).getBytes());
                fileJsonWrite.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else {

            try {
                FileInputStream fileJsonInputStrim = openFileInput(fileName);
                byte[] bytes = new byte[fileJsonInputStrim.available()];
                fileJsonInputStrim.read(bytes);
                fileJsonInputStrim.close();
                String fileJsonString = new String (bytes);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            File historyData = new File(getFilesDir(), "listHistory_file_data.txt");
            if (!historyData.exists()) {
                historyData.createNewFile();
            }

            File userData = new File(getFilesDir(), "user_data.txt");
            if (!userData.exists()) {
                userData.createNewFile();
            }

            File updateFileData = new File(getFilesDir(), "update_file_data.txt");
            if (!updateFileData.exists()) {
                updateFileData.createNewFile();
            }

            File listTestsFileData = new File(getFilesDir(), "listTest_file_data.txt");
            if (!listTestsFileData.exists()) {
                listTestsFileData.createNewFile();
            }

            File usersActionsFileData = new File(getFilesDir(), "usersActions_file_data.txt");
            if (!usersActionsFileData.exists()) {
                usersActionsFileData.createNewFile();
            }

        }

    }

    public String readFile(String fileName) throws IOException {

        File fileJson = new File(getFilesDir(), fileName);
        String fileJsonString = null;
        if (!fileJson.exists()) {


        } else {

            try {
                FileInputStream fileJsonInputStrim = openFileInput(fileName);
                byte[] bytes = new byte[fileJsonInputStrim.available()];
                fileJsonInputStrim.read(bytes);
                fileJsonInputStrim.close();
                fileJsonString = new String (bytes);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fileJsonString;
    }

    public void overWriteFile(String fileName, String fileJsonString) throws IOException {

        File fileJson = new File(getFilesDir(), fileName);
        if (!fileJson.exists()) {


        } else {

            FileOutputStream fileJsonWrite = openFileOutput(fileName, MODE_PRIVATE);
            fileJsonWrite.write(fileJsonString.getBytes());
            fileJsonWrite.close();
        }

    }

    public static String readAssestsFile(AssetManager manager, String fileName)
    {
        byte[] buffer = null;
        try{
            InputStream in = manager.open(fileName);
            int size = in.available();
            buffer = new byte[size];
            in.read(buffer);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new String(buffer);
    }


    public void DownloadFiles(final JSONArray jsonNewFiles, final BridgeWebView webView, final String jsonNewString, final String jsonUpdateFileData) throws JSONException {
        ThinDownloadManager downloadManager = new ThinDownloadManager(2);

        final double[] count = {0};

        DownloadStatusListener downloadListener = new DownloadStatusListener() {

            @Override
            public void onDownloadComplete(int id) {
                try {

                    count[0]++;
                    JSONObject jsonObjCount = new JSONObject();

                    if (count[0] != jsonNewFiles.length()){
                        jsonObjCount.put("COUNT_UPDATE_FILES", count[0]);
                    } else {
                        overWriteFile("json_data.txt", jsonNewString);
                        overWriteFile("update_file_data.txt", jsonUpdateFileData);
                        jsonObjCount.put("UPDATE", "END");

                        Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(mainActivity);
                    }

                    webView.send(jsonObjCount.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDownloadFailed(int id, int errorCode, String errorMessage) {

            }

            @Override
            public void onProgress(int id, long totalBytes, long arg3, int progress) {

            }

        };

        if (jsonNewFiles.length() > 0) {
            webView.send("{\"UPDATE\":\"GO\"}");
        }

        for (int i = 0 ; i < jsonNewFiles.length(); i++) {
            String fileUrl = jsonNewFiles.getString(i);
            Uri downloadUri = Uri.parse(fileUrl);
            String fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
            Uri destinationUri = Uri.parse( getFilesDir() + "/" + fileName);

            DownloadRequest downloadRequest = new DownloadRequest(downloadUri).setDestinationURI(destinationUri);

            downloadManager.add(downloadRequest);

            downloadRequest.setDownloadListener(downloadListener);

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        JSONObject jsonObj = new JSONObject();

        try {
            this.copyBaseFile("json_data.txt", "template/json_data_start.txt");
            this.copyBaseFile("welcome_local.pdf", "template/welcome_local.pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            String fileJsonString = this.readFile("json_data.txt");
            String fileUserJsonString = this.readFile("user_data.txt");
            String fileUpdateFileJsonString = this.readFile("update_file_data.txt");
            String fileHistoryJsonString = this.readFile("listHistory_file_data.txt");
            String fileTestsAnswersJsonString = this.readFile("listTest_file_data.txt");

            jsonObj.put("JSON_STRING_USER", fileUserJsonString);
            jsonObj.put("JSON_STRING", fileJsonString);
            jsonObj.put("USER_LANG", "RU");
            jsonObj.put("UPDATE_FILE", fileUpdateFileJsonString);
            jsonObj.put("STRING_HISTORY", fileHistoryJsonString);
            jsonObj.put("STRING_HISTORY", fileHistoryJsonString);
            jsonObj.put("DATA_FROM_TESTRESULT", fileTestsAnswersJsonString);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final BridgeWebView webView = (BridgeWebView) findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.getAllowContentAccess();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            webSettings.getAllowFileAccessFromFileURLs();
            webSettings.getAllowUniversalAccessFromFileURLs();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setWebContentsDebuggingEnabled(true);
        }
        webSettings.getAllowFileAccess();
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl("file:///android_asset/template/index.html");

        webView.send(jsonObj.toString());

        try {
            // Send Stat in init App
            this.sendStat();
        } catch (IOException e) {
            e.printStackTrace();
        }

        webView.registerHandler("SyncDB", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
            JSONArray jsonNewFiles = null;
            String jsonUpdateFileData = "";
            try {
                JSONObject syncData = new JSONObject(data);
                String jsonNewString = syncData.getString("NEW_JSON_STRING");
                jsonUpdateFileData = syncData.getJSONObject("UPDATE_DATA").getString("DATA_FOR_NEW_POINT");
                jsonNewFiles = syncData.getJSONObject("UPDATE_DATA").getJSONArray("FILES");
                DownloadFiles(jsonNewFiles, webView, jsonNewString, jsonUpdateFileData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            }
        });

        webView.registerHandler("userUpdate", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
            try {
                overWriteFile("user_data.txt", data);
            } catch (IOException e) {
                e.printStackTrace();
            }
            }
        });

        webView.registerHandler("clearFileTestResult", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {

            try {
                overWriteFile("listTest_file_data.txt", "");
            } catch (IOException e) {
                e.printStackTrace();
            }
            }
        });

        webView.registerHandler("settingsShow", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {

            String remoteWelcomeFile = "welcome_remote.pdf";
            String welcomeFile = "template/welcome_local.pdf";

            File file = new File(getFilesDir(), remoteWelcomeFile);
            if (file.exists()) {
                welcomeFile = remoteWelcomeFile;
            }

            Intent pdfViewer = new Intent(getApplicationContext(), PdfViewerActivity.class);
            pdfViewer.putExtra("fileName", welcomeFile);
            startActivity(pdfViewer);

            }
        });

        webView.registerHandler("addHistory", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                try {
                    overWriteFile("listHistory_file_data.txt", data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        webView.registerHandler("userExit", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {
                try {
                    overWriteFile("user_data.txt", "");

                    Intent mainViewer = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(mainViewer);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        webView.registerHandler("addUsersAction", new BridgeHandler() {
            @Override
            public void handler(String data, CallBackFunction function) {

                if (data.length() > 0) {
                    try {
                        String usersActionsData = readFile("usersActions_file_data.txt");
                        overWriteFile("usersActions_file_data.txt", usersActionsData + "||" + data);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        webView.registerHandler("fileClick", new BridgeHandler()
        {
            @Override
            public void handler(String data, CallBackFunction function) {
                try {
                    JSONObject fileData = new JSONObject(data);
                    String urlFile = fileData.getString("URL");
                    String titleFile = fileData.getString("TITLE");

                    String fileNameExtension = urlFile.substring(urlFile.lastIndexOf('.') + 1);

                    if (fileNameExtension.toLowerCase().equals("pdf")) {
                        Intent pdfViewer = new Intent(getApplicationContext(), PdfViewerActivity.class);
                        pdfViewer.putExtra("fileTitle", titleFile);
                        pdfViewer.putExtra("fileName", urlFile);
                        startActivity(pdfViewer);
                    } else if (fileNameExtension.toLowerCase().equals("zip")) {
                        Intent zipViewer = new Intent(getApplicationContext(), ZipViewerActivity.class);
                        zipViewer.putExtra("fileTitle", titleFile);
                        zipViewer.putExtra("fileName", urlFile);
                        startActivity(zipViewer);
                    } else if (fileNameExtension.toLowerCase().equals("mp4") || fileNameExtension.toLowerCase().equals("mp3")) {
                        Intent videoViewer = new Intent(getApplicationContext(), VideoViewerActivity.class);
                        videoViewer.putExtra("fileTitle", titleFile);
                        videoViewer.putExtra("fileName", urlFile);
                        startActivity(videoViewer);
                    } else if (fileNameExtension.toLowerCase().equals("jpg") || fileNameExtension.toLowerCase().equals("png")) {
                        Intent imgViewer = new Intent(getApplicationContext(), ImgViewerActivity.class);
                        imgViewer.putExtra("fileName", urlFile);
                        imgViewer.putExtra("fileTitle", titleFile);
                        startActivity(imgViewer);
                    } else if (fileNameExtension.toLowerCase().equals("doc") || fileNameExtension.toLowerCase().equals("docx")) {
                        File file = new File(getFilesDir(), urlFile);
                        if (file.exists()) {
                            Uri contentUri = getUriForFile(getBaseContext(), "ru.brandspot.Mass", file);
                            Intent viewIntent = new Intent(Intent.ACTION_VIEW);
                            viewIntent.setDataAndType(contentUri, "application/msword");
                            viewIntent.addFlags(FLAG_GRANT_READ_URI_PERMISSION | FLAG_GRANT_WRITE_URI_PERMISSION);

                            PackageManager packageManager = getPackageManager();
                            List<ResolveInfo> activities = packageManager.queryIntentActivities(viewIntent, 0);
                            boolean isIntentSafe = activities.size() > 0;

                            if (isIntentSafe) {
                                startActivity(viewIntent);
                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(),
                                        "На устройстве нет приложения, которое может открыть файлы этого типа.",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                            }

                        }
                    } else if (fileNameExtension.toLowerCase().equals("ppt") || fileNameExtension.toLowerCase().equals("pptx")) {
                        File file = new File(getFilesDir(), urlFile);
                        if (file.exists()) {
                            Uri contentUri = getUriForFile(getBaseContext(), "ru.brandspot.Mass", file);
                            Intent viewIntent = new Intent(Intent.ACTION_VIEW);
                            viewIntent.setDataAndType(contentUri, "vnd.ms-powerpoint");
                            viewIntent.addFlags(FLAG_GRANT_READ_URI_PERMISSION | FLAG_GRANT_WRITE_URI_PERMISSION);

                            PackageManager packageManager = getPackageManager();
                            List<ResolveInfo> activities = packageManager.queryIntentActivities(viewIntent, 0);
                            boolean isIntentSafe = activities.size() > 0;

                            if (isIntentSafe) {
                                startActivity(viewIntent);
                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(),
                                        "На устройстве нет приложения, которое может открыть файлы этого типа.",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        }
                    } else if (fileNameExtension.toLowerCase().equals("xls") || fileNameExtension.toLowerCase().equals("xlsx")) {
                        File file = new File(getFilesDir(), urlFile);
                        if (file.exists()) {
                            Uri contentUri = getUriForFile(getBaseContext(), "ru.brandspot.Mass", file);
                            Intent viewIntent = new Intent(Intent.ACTION_VIEW);
                            viewIntent.setDataAndType(contentUri, "application/vnd.ms-excel");
                            viewIntent.addFlags(FLAG_GRANT_READ_URI_PERMISSION | FLAG_GRANT_WRITE_URI_PERMISSION);

                            PackageManager packageManager = getPackageManager();
                            List<ResolveInfo> activities = packageManager.queryIntentActivities(viewIntent, 0);
                            boolean isIntentSafe = activities.size() > 0;

                            if (isIntentSafe) {
                                startActivity(viewIntent);
                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(),
                                        "На устройстве нет приложения, которое может открыть файлы этого типа.",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                            }
                        }
                    } else {

                        File file = new File(getFilesDir(), urlFile);
                        if (file.exists()) {
                            Uri contentUri = getUriForFile(getBaseContext(), "ru.brandspot.Mass", file);
                            Intent viewIntent = new Intent(Intent.ACTION_VIEW);
                            viewIntent.setDataAndType(contentUri, "*/*");
                            viewIntent.addFlags(FLAG_GRANT_READ_URI_PERMISSION | FLAG_GRANT_WRITE_URI_PERMISSION);

                            PackageManager packageManager = getPackageManager();
                            List<ResolveInfo> activities = packageManager.queryIntentActivities(viewIntent, 0);
                            boolean isIntentSafe = activities.size() > 0;

                            if (isIntentSafe) {
                                startActivity(viewIntent);
                            } else {
                                Toast toast = Toast.makeText(getApplicationContext(),
                                        "На устройстве нет приложения, которое может открыть файлы этого типа.",
                                        Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                            }

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

//        Timer timerCheckStat = new Timer();
//        TimerTask taskSendStat = new SendStat();
//
//        timerCheckStat.schedule(taskSendStat, 5000, 60 * 30 * 1000);
    }

    @Override
    public void onBackPressed() {

    }

}
